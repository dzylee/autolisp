;This AutoLISP script implements custom AutoCAD commands to automate some tasks
;that I frequently perform in AutoCAD.

;Copyright © 2018 David Z. Y. Lee. All Rights Reserved.
;See 'LICENCE.txt' file for details.


;SOME NOTES ABOUT AUTOLISP'S FEATURES:

;Function overloading is not available in AutoLISP.
;AutoLISP appears to pass arguments by value when supplying arguments to a function.
;A function may be defined within another function, thus making the inner function local and exclusive to the enclosing function.
    ; The proper way to do this is to make the inner function a lambda function (an anonymous function).
    ; The informal way is to define a named function within another function. The compiler will complain about this, which is not desirable.
        ; To do this, the name of the local function must be declared like a local variable in the global function's signature.
;A function can be assigned to a variable by using setq():
    ; Simply assign the function's name (without the parameter parentheses) to the variable. Thereafter, the variable can be used to invoke the function in the same way that the function's name would be used.


;|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SET UP
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|;

;| This global variable is used to back up the default error-handling function,
which is represented by the symbol '*error*'. This backup is used to restore the
default error-handling function to the '*error*' symbol after another error-
handling function has been assigned to the '*error*' symbol. This global
variable is meant to be a constant, even though it's not actually constant. |;
(setq *BACKUP_OF_DEFAULT_ERROR_HANDLER* *error*)


;|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
RUDIMENTARY INTERNAL FUNCTIONS -- MEANT TO BE INVOKED ONLY BY OTHER FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|;

;| Prompts the user to select an entity in the drawing and returns the entity's
name. |;
(defun promptClickEntForEntName(/ userSelectedEntity)
    (setq userSelectedEntity (entsel "Select an object in the drawing...\n"))
    (while (not userSelectedEntity)    ;Check if nil, meaning the user selected nothing.
        (setq userSelectedEntity (entsel "No object selected. Try again...\n"))
    )
    (car userSelectedEntity)    ;Take the first element in the list--the entity's name--and return it.
)

;| Lists out each dotted pair (key-value pair) from an object's (graphical or
non-graphical) DEFINITION DATA on a separate line. The name of the object in
question must be supplied as an argument. |;
(defun listDefData(objectName / objectDefData)
    ;Get the definition data for the object by using the object's name. The
    ;definition data is returned by the entget function as a list of key-value
    ;pairs (in the form of dotted pairs), where the key is a DXF group code:
    (setq objectDefData (entget objectName))

    ;Print each dotted pair in the definition-data list on a separate line:
    (foreach listElement objectDefData   ;Note how the variable 'listElement' didn't have to be declared beforehand, either as a global variable or as a local variable.
        (princ listElement)
        (princ "\n")
    )
)

;| Lists out each PROPERTY supported by an object (graphical or non-graphical)
on a separate line. The name of the object in question must be supplied as an
argument. |;
(defun listProperties(objectName)
    (dumpallproperties objectName 1)
        ;The second argument is actually optional. If left unspecified, it
        ;defaults to 0. If set to 1, as it currently is, it will cause certain
        ;property values that would normally be listed together on the same line
        ;to instead be listed separately. For example, this would be relevant to
        ;properties with coordinate values. Setting this argument to 1 will
        ;cause each component of a coordinate to be displayed on its own line.
)

;| Checks if the supplied argument is a number, and returns T if it is,
otherwise nil. |;
(defun isNumber(n)
    (numberp n)
)

;| Checks if the supplied argument is an integer, and returns T if it is,
otherwise nil. |;
(defun isInt(n)
    (equal (type n) 'INT)
)

;| Checks if the supplied argument is a negative number, and returns T if it is,
otherwise nil. |;
(defun isNegative(n)
    (minusp n)
)

;| Returns the full path to the current file as a string. If the file is new and
hasn't been saved yet, the function returns nil. |;
(defun getFullPathToCurrentFile()
    (if (= (getvar 'DWGTITLED) 1)    ;Has the drawing already been saved to file?
        ;If true, construct a string containing the full path to the file:
        (strcat (getvar 'DWGPREFIX) (getvar 'DWGNAME))
        ;Else:
        nil
    )
)

;| Returns the file extension of the current file as a string. It also works for
new files that haven't been saved yet. |;
(defun getFileNameExtOfCurrentFile()
    (substr (vl-filename-extension (getvar 'DWGNAME)) 2)
)

;| Determines the file-format version of the current file. If the file is new
and hasn't been saved yet, the function returns nil. If the file has already
been saved, the function will check if the INTERNAL VERSION of the file is
valid. If it is valid, the function will return a list containing three strings,
in the following order: the internal version of the file, the AutoCAD release in
which the file-format version was first introduced, and all the AutoCAD releases
that use this file-format version as the default. Otherwise, the function
returns nil. Also, this function will only work for the following file types:
.dwg, .dxf, .dwt, and .dws (it determines the type from the filename extension,
which isn't guaranteed to be correct, of course). For all other file types, it
returns nil. |;
(defun getFileFormat(/ fileType fd versionList internalVersion)
    (if (= (getvar 'DWGTITLED) 1)    ;Has the drawing already been saved to file?
        ;If true, then:
        (progn
            (setq fileType (strcase (getFileNameExtOfCurrentFile) T) )    ;Get the filename extension of the file.
            (setq versionList
                  '( ("AC1027" "2013"   "AutoCAD 2013/2014/2015")
                     ("AC1024" "2010"   "AutoCAD 2010/2011/2012")
                     ("AC1021" "2007"   "AutoCAD 2007/2008/2009")
                     ("AC1018" "2004"   "AutoCAD 2004/2005/2006")
                     ("AC1015" "2000"   "AutoCAD 2000/2000i/2002")
                     ("AC1014" "R14"    "AutoCAD R14")
                     ("AC1012" "R13"    "AutoCAD R13")
                     ("AC1009" "R12"    "AutoCAD R11/12")    ;This entry is a repeat of the next one, but it's needed because the SAVEAS/+SAVEAS prompt can only accept "R12" and not "R11/12".
                     ("AC1009" "R11/12" "AutoCAD R11/12")
                     ("AC1006" "R10"    "AutoCAD R10")
                     ("AC1004" "R9"     "AutoCAD R9")
                     ("AC1003" "R2.60"  "AutoCAD R2.60")
                     ("AC1002" "R2.50"  "AutoCAD R2.50")
                     ("AC1001" "R2.22"  "AutoCAD R2.22")
                     ("AC2.22" "R2.22"  "AutoCAD R2.22")
                     ("AC2.21" "R2.21"  "AutoCAD R2.21")
                     ("AC2.10" "R2.10"  "AutoCAD R2.10")
                     ("AC1.50" "R2.05"  "AutoCAD R2.05")
                     ("AC1.40" "R1.40"  "AutoCAD R1.40")
                     ("AC1.2"  "R1.2"   "AutoCAD R1.2")
                     ("MC0.0"  "R1.0"   "AutoCAD R1.0") )
            )
            (cond
                ;If the filename extension is .dxf:
                ( (= filetype "dxf")
                      (setq fd (open (getFullPathToCurrentFile) "r") )
                      (setq internalVersion (assoc (substr (repeat 8 (read-line fd)) 1 6) versionList))    ;The internal version is the first 6 characters of the 8th line in the file.
                      (close fd)
                )
                ;If the filename extension is .dwg, .dwt, or .dws:
                ( (or (= filetype "dwg") (= filetype "dwt") (= filetype "dws"))
                      (setq fd (open (getFullPathToCurrentFile) "r") )
                      (setq internalVersion (assoc (substr (read-line fd) 1 6) versionList))    ;The internal version is the first 6 characters of the first line in the file.
                      (close fd)
                )
                ;If the filename extension is none of the above:
                ( T
                      (setq internalVersion nil)
                )
            )
            internalVersion    ;Return the internal version.
        )
        ;Else (file has not been saved yet):
        nil
    )
)

;| Takes a number as an argument, rounds it to the nearest integer, and returns
the result as a string with thousands separators (commas are used as the
thousands separator).
WARNING: This function cannot handle input numbers that have an arbitrarily
large number of significant figures--the result will be incorrect if the limit
is exceeded. |;
(defun commaSepNum(i / workingStr finalStr sign)
    (if (isNegative i) ;Check if 'i' is negative.
        ;If true, then:
        (progn
            (setq i (* -1 i))    ;Get rid of the negative sign on the number.
            (setq sign "-")      ;Store the negative sign for later.
        )
        ;Else:
        (setq sign "")    ;Since the number is positive, no sign is needed; just store an empty string.
    )

    (setq workingStr (rtos i 2 0))      ;Round 'i' to the nearest integer, and convert it to a decimal string.
    (setq finalStr "")                  ;Initialize 'finalStr' with an empty string.

    (while (> (strlen workingStr) 3)    ;Check if the working string is more than 3 characters long.
        ;Build up the final string:
        (setq finalStr
              ;Copy the 3 rightmost characters from what's remaining of the
              ;working string and add a comma to the front of them. Then add all
              ;this to the front of the final string:
              (strcat
                      (strcat "," (substr workingStr (- (strlen workingStr) 2)) )
                      finalStr
              )
        )
        ;Discard the 3 rightmost characters from what's remaining of the working
        ;string:
        (setq workingStr
              (substr workingStr 1 (- (strlen workingStr) 3) )
        )
    )
    ;Concatenate the last 1 or 2 characters remaining in the working string to
    ;the front of the final string:
    (setq finalStr (strcat workingStr finalStr))

    ;Add the negative sign back to the number if it had one to begin with:
    (setq finalStr (strcat sign finalStr))
)

;| Displays the 'Select Color' dialog box for the user to choose a colour from.
The function returns what the acad_truecolordlg function would return. If the
user cancels the dialog box, the function returns nil. If the user selects a
colour, the function returns a list containing 1, 2, or 3 dotted pairs,
depending on whether the user selected the color from a colour index, the true-
color palette, or a colour book. The dotted pairs are key-value pairs, where the
key is a DXF group code. The LAST dotted pair in the list always represents the
colour selected.

If the colour chosen is a colour index, the function will return a list
containing 1 dotted pair.
-The key in the dotted pair will be 62, which is the DXF code for colour index.
 The corresponding value will be a number representing the colour's index.

If the colour is a true colour (RGB), the function will return a list containing
2 dotted pairs.
-The key in the second dotted pair will be 420, which is the DXF code for true
 colour. The corresponding value will be a single number representing the colour
 (it's not an RGB triple).
-The key in the first dotted pair will be 62. The corresponding value is the
 colour-index number for the colour that MOST CLOSELY matches the true-colour
 colour in the second pair.

If the colour was chosen from a colour book, the function will return a list
containing 3 dotted pairs.
-The key of the third dotted pair will be 430, which is the DXF code for colour
 book. The corresponding value will be a string representing the name of the
 colour, in the form "<colourBookName>$<colourName>".
-The key of the second dotted pair will be 420. The corresponding value is the
 true-colour EQUIVALENT to the colour-book colour in the third pair.
-The key of the first dotted pair will be 62. The corresponding value is the
 colour-index number for the colour that MOST CLOSELY matches the colour-book
 colour in the third pair. |;
(defun promptColourGUI()
    (acad_truecolordlg '(62 . 256) T '(62 . 7))
        ;The first argument specifies the colour that is selected by default in
        ;the 'Select Color' dialog box. The colour can be specified as an
        ;AutoCAD colour index, a true colour, or a colour-book colour. To choose
        ;the method, you supply the appropriate dotted pair.

        ;The second argument is optional. It is used to enable or disable the
        ;display of the ByLayer and ByBlock buttons. A non-nil value will enable
        ;the display of these buttons, while a nil value will disable it. If
        ;disabling the display of these buttons, the AutoCAD Color Indices 0
        ;(ByBlock) and 256 (ByLayer) cannot be used in the first and third
        ;arguments for this function.

        ;The third argument is optional. It is used to set what colour is
        ;displayed in the "currently selected" preview square when the ByLayer
        ;button is pressed. The "previously selected" preview square also gets
        ;set and fixed to this colour for the duration that the dialog box is
        ;open. The colour can be specified as an AutoCAD colour index, a true
        ;colour, or a colour-book colour. To select the method, you supply the
        ;appropriate dotted pair.
)

;| Prompts the user to specify a colour at the command line. A colour can be
specified by using an AutoCAD Color Index, or by the Index's corresponding name
if it has one; a set of RGB values for a true colour; or a name of a colour-book
colour, in the form <colourBookName>$<colourName>. To specify ByLayer and
ByBlock, enter the names "ByLayer" and "ByBlock", respectively.
    The function returns what the acad_truecolorcli function would return, which
is the same as what the acad_truecolordlg function would return. The only
difference is that to cancel this function, the user presses the Escape key,
instead of clicking a button. See the comments for the promptColourGUI()
function for an explanation of the return values. |;
(defun promptColourCLI()
    (acad_truecolorcli '(62 . 256) T "Specify a colour ")
        ;The first argument specifies the colour that is selected by default if
        ;the user presses the Enter key at the prompt without typing anything.
        ;The colour can be specified as an AutoCAD colour index, a true colour,
        ;or a colour-book colour. To choose the method, you supply the
        ;appropriate dotted pair.

        ;The second argument is optional. It is used to allow or disallow
        ;specifying ByLayer and ByBlock. A non-nil value will allow these values
        ;to be specified, while a nil value will disallow it. If these values
        ;are disallowed, the AutoCAD Color Indices 0 (ByBlock) and 256 (ByLayer)
        ;cannot be used in the first argument for this function.

        ;The third argument is optional. It is used to specify your own message
        ;for the prompt. If it is omitted, the prompt will use the message
        ;"New Color" by default.
)

;| Converts a true-colour value to a set of RGB values, and returns the latter
as a string in the form "r,g,b". A true-colour value is a single positive
decimal integer represented as a 24-bit-long binary number that has the form
rrrrrrrrggggggggbbbbbbbb. Each octet represents one of the three colour
components of red, green, and blue. |;
(defun true->rgb(trueColour / red green blue)
    ;Discard the lowest-order octet, so that only 16 bits remain, with the
    ;form: rrrrrrrrgggggggg
    (setq green (lsh trueColour -8) )
    (setq red green)

    ;Get the value of the green component:
    (setq green (itoa (logand green 255)) )

    ;Get the value of the red component:
    (setq red (itoa (lsh red -8)) )

    ;Get the value of the blue component:
    (setq blue (itoa (logand trueColour 255)) )

    ;Return the RGB values as a comma-separated string:
    (strcat red "," green "," blue)
)


;|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
MATHEMATICS FUNCTIONS -- MEANT TO BE INVOKED BY USER OR BY OTHER FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|;

;| Takes two lists representing vectors, as arguments; then substracts the
second vector from the first to get the resultant vector. The resultant vector
is returned.
WARNING: This function does not validate the input lists to make sure that they
both contain only numbers and have the same number of elements. |;
(defun vectDiff(vector1 vector0 / resultVect)
    (repeat (length vector1)
        ;Subtract the first item in the second list from the first item in the
        ;first list. Then take the result and insert it as an element to the
        ;FRONT of a temporary list:
        (setq resultVect (cons (- (car vector1) (car vector0)) resultVect) )

        ;Discard the first element from both the first and second input lists:
        (setq vector0 (cdr vector0))
        (setq vector1 (cdr vector1))
    )
    ;Reverse the order of the elements in the temporary list, so that the
    ;elements are in the correct order, and then return the result:
    (reverse resultVect)
)

;| Takes two lists representing vectors, as arguments; then adds the two vectors
together to get the resultant vector. The resultant vector is returned. WARNING:
This function does not validate the lists to make sure that they both contain
only numbers and have the same number of elements. |;
(defun vectSum(vector0 vector1 / resultVect)
    (repeat (length vector0)
        ;Add the first item in both lists together. Then take the result and
        ;insert it as an element to the FRONT of a temporary list:
        (setq resultVect (cons (+ (car vector0) (car vector1)) resultVect) )

        ;Discard the first element from both the first and second input lists:
        (setq vector0 (cdr vector0))
        (setq vector1 (cdr vector1))
    )
    ;Reverse the order of the elements in the temporary list, so that the
    ;elements are in the correct order, and then return the result:
    (reverse resultVect)
)

;| Takes a list of lists as an argument, with the nested lists representing
vectors, and then adds all the vectors together to get the resultant vector. The
resultant vector is returned.
WARNING: This function does not validate the nested lists to make sure that they
all contain only numbers and have the same number of elements. |;
(defun vectSumN(listOfVectors / resultVect)
    ;Take a copy of the first vector in the list and store it in a temporary
    ;variable:
    (setq resultVect (car listOfVectors))
    ;Discard the first vector from the list:
    (setq listOfVectors (cdr listOfVectors))

    (repeat (length listOfVectors)
        ;Add the new first vector in the list to the vector in the temporary
        ;variable:
        (setq resultVect (vectSum resultVect (car listOfVectors)) )
        ;Discard the first vector in the list:
        (setq listOfVectors (cdr listOfVectors))
    )
    resultVect
)

;| Takes two lists representing vectors, as arguments; then calculates the dot
product (aka scalar product) of the two vectors. The dot product is returned.
WARNING: This function does not validate the lists to make sure that they both
contain only numbers and have the same number of elements. |;
(defun vectDotProd(vector0 vector1 / result)
    (setq result 0)    ;Initialize the temporary variable to zero.
    (repeat (length vector0)
        ;Multiply the first item from both lists together, then add the result
        ;to the value held in the temporary variable:
        (setq result (+ (* (car vector0) (car vector1)) result) )

        ;Discard the first item from both the first and second lists:
        (setq vector0 (cdr vector0))
        (setq vector1 (cdr vector1))
    )
    result
)

;| Takes a list representing a vector, as an argument; then calculates the
magnitude of the vector. The magnitude of the vector is returned.
WARNING: This function does not validate the list to make sure that it contains
only numbers. |;
(defun vectMag(vector0 / result)
    (setq result 0)    ;Initialize the temporary variable to zero.

    ;Take one value at a time from the list, square the value, and then add the
    ;result to the temporary variable:
    (foreach element vector0
        (setq result (+ (expt element 2) result) )
    )

    ;Take the square root of the final value in the temporary variable, and
    ;return it:
    (sqrt result)
)

;| Calculates the factorial of a positive integer RECURSIVELY, and returns the
resulting number as a Real date type. |;
(defun factRecurs(n)
    (cond
        ( (not (isNumber n)) (princ "Argument is not a number.") (princ) )
        ( (or (not (isInt n)) (isNegative n) ) (princ "Argument is not a positive integer.") (princ) )
        ( T (if (= n 0)    ;Is the number equal to zero?
                ;If true, then:
                1.0
                ;Else:
                (* (float n) (factRecurs (- n 1)) )
            )
        )
    )
)

;| Calculates the factorial of a positive integer ITERATIVELY, and returns the
resulting number as a Real date type. |;
(defun factIter(n / runningTotal)
    (cond
        ( (not (isNumber n)) (princ "Argument is not a number.") (princ) )

        ( (or (not (isInt n)) (isNegative n) ) (princ "Argument is not a positive integer.") (princ) )

        ;If n is zero:
        ( (= n 0) (setq runningTotal 1.0) )

        ;If n is a positive integer greater than zero:
        ( T (setq runningTotal (float n))
            (while (> n 1)
                (setq runningTotal (* runningTotal (1- n)) )
                (setq n (1- n))
            )
            runningTotal
        )
    )
)


;|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CUSTOM AUTOCAD COMMANDS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|;

;| Turns on all layers. |;
(defun c:turnOnAllLayers()
    (command-s "_.-LAYER" "_On" "*" "")
    (princ)    ;Suppress output of preceding expression.
)

;| Unfreezes all layers. |;
(defun c:unfreezeAllLayers()
    (command-s "_.-LAYER" "_Thaw" "*" "")
    (princ)    ;Suppress output of preceding expression.
)

;| Unlocks all layers. |;
(defun c:unlockAllLayers()
    (command-s "_.-LAYER" "_Unlock" "*" "")
    (princ)    ;Suppress output of preceding expression.
)

;| Turns on, unfreezes, and unlocks all layers. |;
(defun c:freeAllLayers()
    (command-s "_.UNDO" "_Begin")    ;Mark the beginning of the UNDO group.

    (command-s "_.-LAYER" "_Unlock" "*" "")
    (command-s "_.-LAYER" "_Thaw" "*" "")
    (command-s "_.-LAYER" "_On" "*" "")

    (command-s "_.UNDO" "_End")      ;Mark the end of the UNDO group.
    (princ)    ;Suppress output of preceding expression.
)

;| Prints to the AutoCAD Text Window the DEFINITION DATA of the user-selected
entity. |;
(defun c:dumpDefData(/ entityName)
    (setq entityName (promptClickEntForEntName))    ;Make the user select an entity in the drawing in order to get its name.
    (princ "\n")

    (textpage)    ;Open the AutoCAD Text Window and give it the focus.

    (princ "DEFINITION DATA:\n")
    (listDefData entityName)    ;Print the DEFINITION DATA of the selected entity.
    (princ)    ;Suppress output of preceding expression.
)

;| Prints to the AutoCAD Text Window the DEFINITION DATA and the PROPERTIES of
the user-selected entity. |;
(defun c:dumpAllEntInfo(/ entityName)
    (setq entityName (promptClickEntForEntName))    ;Make the user select an entity in the drawing in order to get its name.
    (princ "\n")

    (textpage)    ;Open the AutoCAD Text Window and give it the focus.

    (princ "DEFINITION DATA:\n")
    (listDefData entityName)    ;Print the DEFINITION DATA of the selected entity.
    (princ "\n")

    (princ "PROPERTIES:\n")
    (listProperties entityName)    ;Print the PROPERTIES of the selected entity.
    (princ)    ;Suppress output of preceding expression.
)

;| Performs a full, quick save on the current file, while maintaining the file
type and AutoCAD file-format version, and then prints the size of the reduction
in bytes and as a percentage, followed by the new file size. If the current file
hasn't already been saved earlier, the user will be presented with a dialog box
to choose a location, file name, file format, and file-format version for the
current file. Once the user clicks the Save button, a full save is performed,
and then the size in bytes of the newly created file is printed. |;
(defun c:fqsave(/ pathToCurrentFile filenameExt drawingFormat
                  originalFileSize newFileSize fileSizeDifference
                  original_THUMBSAVE_value
                  original_EXPERT_value
                  original_FILEDIA_value
                  original_ISAVEPERCENT_value)
    (if (= (getvar 'DWGTITLED) 1)    ;Has the drawing already been saved to file? 1 means yes, 0 means no.
        ;If true, then:
        (progn
            (setq pathToCurrentFile (getFullPathToCurrentFile))          ;Get the full path to the current file.
            (setq originalFileSize (vl-file-size pathToCurrentFile))     ;Get the size of the current file before it's saved.
            (setq filenameExt (getFileNameExtOfCurrentFile))             ;Get the filename extension of the current file.
            (setq drawingFormat (nth 1 (getFileFormat)) )                ;Get the file-format version of the current file.

            (setq original_THUMBSAVE_value (getvar 'THUMBSAVE))          ;Back up the original value in the THUMBSAVE system variable.
            (setvar "THUMBSAVE" 0)                                       ;Set the THUMBSAVE system variable to 0, which disables saving thumbnail preview images in the file.

            (setq original_EXPERT_value (getvar 'EXPERT))                ;Back up the original value in the EXPERT system variable.
            (setvar "EXPERT" 2)                                          ;Set the EXPERT system variable to 2, which disables the file-overwrite warning.

            (setq original_FILEDIA_value (getvar 'FILEDIA))              ;Back up the original value in the FILEDIA system variable.
            (setvar "FILEDIA" 0)                                         ;Set the FILEDIA system variable to 0, which disables the display of file-navigation dialog boxes.

            (setq original_ISAVEPERCENT_value (getvar 'ISAVEPERCENT))    ;Back up the original value in the ISAVEPERCENT system variable.
            (setvar "ISAVEPERCENT" 0)                                    ;Set the ISAVEPERCENT system variable to 0%, which effectively turns off incremental saving.

            ;Save the current file:
            (cond
                ;If the current file is a .dwg file:
                ( (= filenameExt "dwg") (command-s "_.+SAVEAS" filenameExt drawingFormat "" "") )

                ;If the current file is a .dxf file:
                ( (= filenameExt "dxf") (command-s "_.+SAVEAS" filenameExt drawingFormat "" "16" "") )

                ;If the current file is a .dwt file:
                ( (= filenameExt "dwt") (command-s "_.+SAVEAS" filenameExt "_Template" pathToCurrentFile "" "") )

                ;If the current file is a .dws file:
                ( (= filenameExt "dws") (command-s "_.+SAVEAS" filenameExt "_Standards" "") )
            )

            ;Restore the ISAVEPERCENT, FILEDIA, EXPERT, and THUMBSAVE system
            ;variables to their original values:
            (setvar "ISAVEPERCENT" original_ISAVEPERCENT_value)
            (setvar "FILEDIA" original_FILEDIA_value)
            (setvar "EXPERT" original_EXPERT_value)
            (setvar "THUMBSAVE" original_EXPERT_value)

            (setq newFileSize (vl-file-size pathToCurrentFile))    ;Get the size of the current file after it's saved.

            ;Calculate and print the size of the reduction and the new file
            ;size:
            (princ "\nFile size reduced by ")
            (princ (commaSepNum                                                      ;Format the result of the subtraction by inserting thousands separators.
                       (setq fileSizeDifference (- originalFileSize newFileSize))    ;Calculate the difference between the 'before' and 'after' file sizes.
                   )
            )
            (princ " bytes (")
            (princ (rtos (* (/ (float fileSizeDifference) (float originalFileSize)) 100) 2 3) )    ;Calculate reduction as a percentage to 3 decimal places.
            (princ "% reduction). New file size is ")
            (princ (commaSepNum newFileSize))
            (princ " bytes.")
        )
        ;Else (The drawing has not already been saved to file):
        (progn
            (setq original_FILEDIA_value (getvar 'FILEDIA))              ;Back up the original value in the FILEDIA system variable.
            (setvar "FILEDIA" 1)                                         ;Set the FILEDIA system variable to 1, which enables the display of file-navigation dialog boxes.

            (setq original_ISAVEPERCENT_value (getvar 'ISAVEPERCENT))    ;Back up the original value in the ISAVEPERCENT system variable.
            (setvar "ISAVEPERCENT" 0)                                    ;Set the ISAVEPERCENT system variable to 0%, which effectively turns off incremental saving.

            (command-s "_.SAVEAS" "" "~")                                ;Open a dialog box (that's what the tilde is for) for the user to save the current file.

            (setvar "ISAVEPERCENT" original_ISAVEPERCENT_value)          ;Restore the ISAVEPERCENT system variable to its original value.
            (setvar "FILEDIA" original_FILEDIA_value)                    ;Restore the FILEDIA system variable to its original value.

            (if (= (getvar 'DWGTITLED) 1)    ;Check if the user clicked the Save button.
                ;If true, then:
                (progn
                    (setq newFileSize (vl-file-size (getFullPathToCurrentFile)) )    ;Get the size of the current file after it's saved.

                    ;Print the size of the file:
                    (princ "\nFile size is ")
                    (princ (commaSepNum newFileSize))    ;Format the number by inserting thousands separators.
                    (princ " bytes.")
                )
                ;Else (The user clicked the Cancel button):
                (princ "\nFile not saved.")
            )
        )
    )
    (princ)    ;Suppress output of preceding expression.
)

;| Measures and prints, in ARCHITECTURAL and FRACTIONAL format, the distance
between two points in three-dimensional space, plus prints the difference
between both points' respective X components, Y components, and Z components.
The relevant system variables for this function are:
LUNITS   -- set to 4 or 5 (via the third argument of the rtos() function)
LUPREC   -- set to 8 (via the fourth argument of the rtos() function)
UNITMODE -- set to 0
DIMZIN   -- set to 3 |;
(defun c:afdistance(/ pt0 pt1
                      original_UNITMODE_value
                      original_DIMZIN_value)
    (graphscr)    ;Switch focus to the drawing screen, whether that's model space or paper space.

    (setq pt0 (getpoint "Pick, or enter the coordinates x,y,z for, a starting point: "))
    (princ "\n")
    (setq pt1 (getpoint pt0 "Pick, or enter the coordinates x,y,z for, a second point: "))
    (princ "\n")

    ;Calculate the difference between the corresponding components of vectors
    ;pt0 and pt1; i.e. subtract vector pt0 from vector pt1. Then reuse variable
    ;pt0 by storing the result from the subtraction in it:
    (setq pt0 (vectDiff pt1 pt0))

    ;Calculate the distance between points pt0 and pt1. Then reuse variable pt1
    ;by storing the distance in it:
    (setq pt1 (vectMag pt0))

    (setq original_UNITMODE_value (getvar 'UNITMODE))    ;Back up the original value in the UNITMODE system variable.
    (setvar "UNITMODE" 0)                                ;Set the UNITMODE system variable to 0, which further controls the format for architectural and fractional units.

    (setq original_DIMZIN_value (getvar 'DIMZIN))        ;Back up the original value in the DIMZIN system variable.
    (setvar "DIMZIN" 3)                                  ;Set the DIMZIN system variable to 3, which suppresses the display of "0'" when there are 0 feet, but displays "0"" when is there are 0 inches.

    ;Print the distance between the two points:
    (princ "Distance = ")
    (princ (rtos pt1 4 8) )          ;Print the distance in architectural format (LUNITS=4) with the highest precision possible (LUPREC=8).
    (princ " (")
    (princ (rtos pt1 5 8) )          ;Print the distance in fractional format (LUNITS=5) with the highest precision possible (LUPREC=8).
    (princ "\" = ")
    (princ (rtos pt1 2 3) )          ;Print the distance in decimal inches (LUNITS=2) with 3 decimal places of precision (LUPREC=3).
    (princ "\")  >>  (pt2-pt1)  //  ")

    ;Print difference between the X components of the two points:
    (princ "dX = ")
    (princ (rtos (nth 0 pt0) 4 8) )  ;Print delta X in architectural format (LUNITS=4) with the highest precision possible (LUPREC=8).
    (princ " (")
    (princ (rtos (nth 0 pt0) 5 8) )  ;Print delta X in fractional format (LUNITS=5) with the highest precision possible (LUPREC=8).
    (princ "\" = ")
    (princ (rtos (nth 0 pt0) 2 3) )  ;Print delta X distance in decimal inches (LUNITS=2) with 3 decimal places of precision (LUPREC=3).
    (princ "\")  //  ")

    ;Print difference between the Y components of the two points:
    (princ "dY = ")
    (princ (rtos (nth 1 pt0) 4 8) )  ;Print delta Y in architectural format (LUNITS=4) with the highest precision possible (LUPREC=8).
    (princ " (")
    (princ (rtos (nth 1 pt0) 5 8) )  ;Print delta Y in fractional format (LUNITS=5) with the highest precision possible (LUPREC=8).
    (princ "\" = ")
    (princ (rtos (nth 1 pt0) 2 3) )  ;Print delta Y distance in decimal inches (LUNITS=2) with 3 decimal places of precision (LUPREC=3).
    (princ "\")  //  ")

    ;Print difference between the Z components of the two points:
    (princ "dZ = ")
    (princ (rtos (nth 2 pt0) 4 8) )  ;Print delta Z in architectural format (LUNITS=4) with the highest precision possible (LUPREC=8).
    (princ " (")
    (princ (rtos (nth 2 pt0) 5 8) )  ;Print delta Z in fractional format (LUNITS=5) with the highest precision possible (LUPREC=8).
    (princ "\" = ")
    (princ (rtos (nth 2 pt0) 2 3) )  ;Print delta Z distance in decimal inches (LUNITS=2) with 3 decimal places of precision (LUPREC=3).
    (princ "\")  //  ")

    (setvar "UNITMODE" original_UNITMODE_value)    ;Restore the UNITMODE system variable to its original value.
    (setvar "DIMZIN" original_DIMZIN_value)        ;Restore the DIMZIN system variable to its original value.

    (princ)    ;Suppress output of preceding expression.
)

;|
In AutoCAD terminology, "running object snaps" refers to the object-snap modes
that are enabled in the Object Snap and 3D Object Snap tabs of the Drafting
Settings dialog box.
|;

;| These two global variables store the two sums of values for the 2D- and 3D-
object-snap modes, respectively, that I usually use myself. Added to each sum
are the respective values used to turn on 2D and 3D object snapping. These two
global variables are meant to be used like constants (even though they aren't
actually constant). If the object-snap modes I normally use changes in the
future, edit the values assigned to these two variables and recompile the
program.
    NOTE: The OSMODE system variable stores both the enabled/disabled state and
all the enabled object-snap modes for 2D object snapping, by using a single
number that sums together each setting's unique numerical value. The
enabled/disabled state and each object-snap mode is its own separate setting.
This means that 2D object snapping can be enabled while some or all the object-
snap modes are disabled. The converse is true as well: 2D object snapping can be
disabled while some or all the object-snap modes are enabled. The 3DOSMODE
system variable does the same for 3D object snapping and works in the same
fashion as the OSMODE system variable.
    In the case of OSMODE, to enable 2D object snapping, add 0 to the sum of all
the 2D-object-snap-mode values; and to disable, add 16384. The value 1024 is
assigned to the 'Quick' object-snap mode which is obsolete, so adding it to the
sum has the same effect as adding 0; but it is best to avoid using 1024, in case
its status changes in the future.
    In the case of 3DOSMODE, to enable 3D object snapping, add 0 to the sum of
all the 3D-object-snap-mode values; and to disable, add 1. |;
(setq *MY_USUAL_2D_OSNAP_SETTINGS* 6311)
(setq *MY_USUAL_3D_OSNAP_SETTINGS* 38)

;| These two global variables are used to back up the on/off states and the
object-snap modes that the USER LAST specified, for 2D and 3D object snapping,
respectively. They are used to restore the states and the modes when an AutoCAD
command or AutoLISP script alters these settings and, as a result of being
terminated before finishing, fails to restore them. |;
(setq *backup_of_OSMODE_value* (getvar 'OSMODE))
(setq *backup_of_3DOSMODE_value* (getvar '3DOSMODE))

;| Turns on 2D and 3D object snapping and sets only the object-snap modes that I
usually use, for both 2D and 3D object snapping. |;
(defun c:myosm()    ;"My object-snap modes".
    ;Turn on 2D and 3D object snapping and set only the object-snap modes that I
    ;usually use:
    (setvar "OSMODE" *MY_USUAL_2D_OSNAP_SETTINGS*)
    (setvar "3DOSMODE" *MY_USUAL_3D_OSNAP_SETTINGS*)

    ;Back up the newly set on/off states and object-snap modes:
    (setq *backup_of_OSMODE_value* *MY_USUAL_2D_OSNAP_SETTINGS*)
    (setq *backup_of_3DOSMODE_value* *MY_USUAL_3D_OSNAP_SETTINGS*)

    (princ)    ;Suppress output of preceding expression.
)

;| Opens the Drafting Settings dialog box to the 2D Object Snap tab to let the
user turn on or off object snapping and choose which object-snap modes to enable
or disable. Then backs up the new settings. |;
(defun c:set2dosm()    ;"Set 2D-object-snap modes".
    ;Open the Drafting Settings dialog box to the Object Snap tab (tab #2):
    (command-s "_.+DSETTINGS" "2")

    ;Back up the newly set on/off states and object-snap modes:
    (setq *backup_of_OSMODE_value* (getvar 'OSMODE))
    (setq *backup_of_3DOSMODE_value* (getvar '3DOSMODE))

    (princ)    ;Suppress output of preceding expression.
)

;| Opens the Drafting Settings dialog box to the 3D Object Snap tab to let the
user turn on or off object snapping and choose which object-snap modes to enable
or disable. Then backs up the new settings. |;
(defun c:set3dosm()    ;"Set 3D-object-snap modes".
    ;Open the Drafting Settings dialog box to the 3D Object Snap tab (tab #5):
    (command-s "_.+DSETTINGS" "5")

    ;Back up the newly set on/off states and object-snap modes:
    (setq *backup_of_OSMODE_value* (getvar 'OSMODE))
    (setq *backup_of_3DOSMODE_value* (getvar '3DOSMODE))

    (princ)    ;Suppress output of preceding expression.
)

;| Restores the on/off states and the object-snap modes that were LAST set by
the USER, for both 2D and 3D object snapping. |;
(defun c:restosm()    ;"Restore object-snap modes".
    (setvar "OSMODE" *backup_of_OSMODE_value*)
    (setvar "3DOSMODE" *backup_of_3DOSMODE_value*)

    (princ)    ;Suppress output of preceding expression.
)

;| Error-handling function to use with the c:cpfit() custom command. |;
(defun cpfit_error_handler(errorMessage)
    (command-s "_.UNDO" "_End")                         ;Mark the end of the UNDO group.

    (command-s "_.U")                                   ;Undo by 1 step.

    (setvar "COPYMODE" original_COPYMODE_value)         ;Restore the COPYMODE system variable to its original value.

    ;Print the error message:
    (princ "\nerror: ")
    (princ errorMessage)
    (princ "\n")

    (setq *error* *BACKUP_OF_DEFAULT_ERROR_HANDLER*)    ;Restore the default error-handling function back to the '*error*' symbol.

    (princ)    ;Suppress output of preceding expression.
)

;| Creates a simple, linear array of any graphical object in the drawing by
copying it and then spacing out the copies equally between two points. |;
(defun c:cpfit(/ original_COPYMODE_value selSet)
    (*push-error-using-stack*)                                        ;Enable access to the local variables of this function from within the error-handling function.
    (setq *error* cpfit_error_handler)                                ;Specify the custom error-handling function to use for c:cpfit() by setting the '*error*' symbol to the error handler.

    (command-s "_.UNDO" "_Begin")                                     ;Mark the beginning of the UNDO group. The UNDO group is not for the COPY command that comes later, but for the setvar() functions.

    (setq original_COPYMODE_value (getvar 'COPYMODE))                 ;Back up the original value in the COPYMODE system variable.
    (setvar "COPYMODE" 1)                                             ;Set the COPYMODE system variable to permit the COPY command to perform its operation only once.

    (graphscr)                                                        ;Switch focus to the drawing screen, whether that's model space or paper space.
    (setq selSet (ssget))                                             ;Prompt the user to select the objects to array.
    (command "_.COPY" selSet "" PAUSE "_Array" PAUSE "_Fit" PAUSE)    ;Invoke the COPY command to create an array of the selected entities, and space out the copies by fit.

    (command-s "_.UNDO" "_End")                                       ;Mark the end of the UNDO group.

    (setvar "COPYMODE" original_COPYMODE_value)                       ;Restore the COPYMODE system variable to its original value.

    (setq *error* *BACKUP_OF_DEFAULT_ERROR_HANDLER*)                  ;Restore the default error-handling function back to the '*error*' symbol.
    (*pop-error-mode*)                                                ;End the use of the *push-error-using-stack* function.

    (princ)    ;Suppress output of preceding expression.
)

;| Rotates selected objects by using a reference line specified by the user. |;
(defun c:ror()
    (graphscr)                                            ;Switch focus to the drawing screen, whether that's model space or paper space.
    (command "_.ROTATE" (ssget) "" PAUSE "_Reference")    ;Invoke the ROTATE command to rotate using a user-specified reference line.
    (princ)    ;Suppress output of preceding expression.
)

;| Changes view to a plan view based on the world coordinate system.
NOTE: This command only works in model space. |;
(defun c:wplan()
    (graphscr)    ;Switch focus to the drawing screen, whether that's model space or paper space.
    (command-s "_.PLAN" "_World")
    (princ)       ;Suppress output of preceding expression.
)
