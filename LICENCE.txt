LICENCE INFORMATION

Licence last updated: October 26, 2019


The following stipulations apply to all versions of this program and to all
revisions of this program's source code.

With regard to this program, you are permitted only to read this program's
source code, and nothing else. To elaborate, you may not use, make copies of,
distribute, publish, nor create any derivative work from this program or any
part of its source code for any purpose, whether personal, commercial, or
otherwise. You may not make any revisions to this program's source code. You
may not claim this program or any part of its source code as your own work.

This licence is subject to change without notice at any time in the future.
